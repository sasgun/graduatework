from flask import Flask
from flask.json import JSONEncoder
from datetime import date
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from prometheus_flask_exporter import PrometheusMetrics

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


app = Flask(__name__)

from app import routes

metrics = PrometheusMetrics(app)

app.json_encoder = CustomJSONEncoder

app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
migrate = Migrate(app, db)



class Statistic(db.Model):
    __tablename__ = 'statistic'
    __table_args__ = (db.UniqueConstraint('date_value', 'country_code', name='i_statistic'),
                      {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8mb4',
                       }
                      )
    # the columns of the table.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date_value = db.Column(db.Date)
    country_code = db.Column(db.String(3))
    confirmed = db.Column(db.Integer)
    deaths = db.Column(db.Integer)
    stringency_actual = db.Column(db.Integer)
    stringency = db.Column(db.Integer)
