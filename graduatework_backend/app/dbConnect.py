# import logging
from concurrent.futures import ThreadPoolExecutor

from app import config
import pymysql


# import logging

# logging.basicConfig(handlers=[logging.FileHandler('sql_resul.log', 'a', 'utf-8')], format=' %(asctime)s - %(
# message)s', level=logging.ERROR, datefmt='%d-%b-%y %H:%M:%S')
# logging.basicConfig(handlers=[logging.FileHandler('app.log', 'a', 'utf-8')], format=' %(asctime)s - %(message)s',
#                     level=logging.ERROR, datefmt='%d-%b-%y %H:%M:%S')


def create_connection():
    return pymysql.connect(
        host=config.host,
        db=config.db_name,
        user=config.username,
        password=config.password,
        cursorclass=pymysql.cursors.DictCursor,
        autocommit=True
    )


def insert_data(res):
    connection = create_connection()


    sql = "replace into statistic  (date_value, country_code, confirmed, deaths, stringency_actual, stringency) VALUES (%s, %s, %s, %s, %s, %s )"

    try:
        cursor = connection.cursor()

        for date, value in res.items():
            val = value
            for country, statistic in val.items():
                with ThreadPoolExecutor(16) as executor:
                    result_insert = executor.map( cursor.execute(sql, (date, country, statistic['confirmed'], statistic['deaths'], statistic['stringency_actual'], statistic['stringency'])) )
                    # print(result_insert)
                    connection.commit()
        # print(domain)

        # result = cursor.fetchall()
        # print(type(result))
        # print(result)
        # connection.commit()
        cursor.close()
        return "result"
    finally:
        connection.close()


def select_10day_data():
    sql = "select date_value, country_code, confirmed, deaths, stringency_actual, stringency from statistic  where date_value > DATE_SUB(NOW(), INTERVAL 30 DAY) order by date_value;"
    connection = create_connection()
    try:
        cursor = connection.cursor()

        cursor.execute(sql, )
        result = cursor.fetchall()
        # print(type(result))
        # print(result)

        cursor.close()
        return result
    finally:
        connection.close()


def select_all_data():
    sql = "select date_value, country_code, confirmed, deaths, stringency_actual, stringency from statistic  order by date_value;"
    connection = create_connection()
    try:
        cursor = connection.cursor()

        cursor.execute(sql, )
        result = cursor.fetchall()
        # print(type(result))
        # print(result)

        cursor.close()
        return result
    finally:
        connection.close()
