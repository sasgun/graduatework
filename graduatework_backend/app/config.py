# Mysql access example
#host = "192.168.56.110"
#username = "graduate"
#password = "PASSWORD"
#db_name = "graduate_work"

import os

FLASK_ENV = os.environ['enviroment']
host = os.environ['DB_HOST']
username = os.environ['DB_USER']
password = os.environ['DB_PASSWORD']
db_name = os.environ['DATABASE']
# print("Running with user: %s" % username)

value = host.split(':')

host = value[0]


SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + username + ':' + password + '@' + host + ':3306/' + db_name + '?charset=utf8mb4'
MYSQL_DATABASE_CHARSET = 'utf8mb4'
