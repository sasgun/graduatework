from datetime import datetime
import urllib

from flask import render_template, request, redirect, jsonify
from werkzeug.exceptions import BadRequestKeyError
from socket import timeout

import re
import json
import urllib.request
from concurrent.futures import ThreadPoolExecutor
import ssl
import validators
import logging

from app import app
from app import dbConnect


@app.route('/api/v1/10day_data')
def get_data():  # put application's code here
    data = dbConnect.select_10day_data()
    return jsonify(data);

@app.route('/api/v1/all_data')
def get_all_data():  # put application's code here
    data = dbConnect.select_all_data()
    return jsonify(data);

@app.route('/api/v1/update', methods=['GET'])
def update_data():  # put application's code here
    datetimeobj = datetime.now()
    datestr = datetimeobj.strftime("%Y-%m-%d ")
    url = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2022-04-20/" + datestr
    datetimeob = datetime.now()
    timestampstr = datetimeob.strftime("%d-%b-%Y (%H:%M:%S.%f)")
    print('Current Timestamp : ', timestampstr)
    print(url)
    response = urllib.request.urlopen(url, timeout=100)
    data = json.loads(response.read())
    res = data['data']
    dbConnect.insert_data(res)
    return "Success";

@app.route('/api/v1/update_all', methods=['GET'])
def update_all_data():  # put application's code here
    datetimeobj = datetime.now()
    datestr = datetimeobj.strftime("%Y-%m-%d ")
    url = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2022-01-01/" + datestr
    datetimeob = datetime.now()
    timestampstr = datetimeob.strftime("%d-%b-%Y (%H:%M:%S.%f)")
    print('Current Timestamp : ', timestampstr)
    print(url)
    response = urllib.request.urlopen(url, timeout=100)
    data = json.loads(response.read())
    res = data['data']
    dbConnect.insert_data(res)
    return "Success";

@app.route('/healthz')
def healthz():
    return "OK"
