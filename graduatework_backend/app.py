from app import app
from flask.json import JSONEncoder
from datetime import date
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


if __name__ == '__main__':
    app.json_encoder = CustomJSONEncoder
    app.run(host='0.0.0.0', port=3000)
