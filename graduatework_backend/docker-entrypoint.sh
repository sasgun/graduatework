#!/bin/bash

# Apply database migrations
echo "Apply database migrations"
flask db migrate
flask db upgrade

echo "Run  application"

uwsgi --ini uwsgi.ini


