output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = data.aws_eks_cluster.cluster.endpoint 
}

output "region" {
  description = "AWS region"
  value       = var.region
}

output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = var.cluster_name
}

output "cluster_cert_data" {
  sensitive   = true
  description = "Kubernetes Cluster certificate authority data"
  value       = data.aws_eks_cluster.cluster.certificate_authority[0].data
}

output "gitlab_token_develop" {
  sensitive   = true
  description = "GitLab k8s develop service account token"
  value       = lookup(data.kubernetes_secret.gitlab_sa_secret_develop.data, "token")
}

output "gitlab_token_production" {
  sensitive   = true
  description = "GitLab k8s production  service account token"
  value       = lookup(data.kubernetes_secret.gitlab_sa_secret_production.data, "token")
}
