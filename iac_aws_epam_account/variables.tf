variable "project" {
  default     = "graduation-work-325219"
}

variable "region" {
  default     = "eu-central-1"
}


variable "aws_vpc_id" {
description = "exist EPAM VPC"
type = string
default = "vpc-6c6dfe06"
}

variable "subnet_ids" {
description = "Exist EPAM VPC subnet IDs"
default =["subnet-18068254", "subnet-dc4a30b6", "subnet-2965d455"]
}

variable "sg_id" {
description = "exist EPAM SG"
type = string
default = "sg-0a8b5db2a7dcca42a"
}

variable "cluster_name" {
  default     = "graduate-k8s-cluster"
}

variable "node_count" {
  default     = "3"
}

variable "ingressnode_count" {
  default     = "2"
}


variable "db_user" {
  default     = "test"
}

variable "db_pass" {
  default     = "testdfhjdsvdl!"
}

variable "db_name" {
  type    = string
  default = "gradiate_db"
}

variable "namespaces" {
  type    = set(string)
  default = ["production", "develop"]
}

