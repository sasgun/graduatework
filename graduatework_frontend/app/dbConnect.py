# import logging

from app import config
import pymysql


# import logging

# logging.basicConfig(handlers=[logging.FileHandler('sql_resul.log', 'a', 'utf-8')], format=' %(asctime)s - %(
# message)s', level=logging.ERROR, datefmt='%d-%b-%y %H:%M:%S')
# logging.basicConfig(handlers=[logging.FileHandler('app.log', 'a', 'utf-8')], format=' %(asctime)s - %(message)s',
#                     level=logging.ERROR, datefmt='%d-%b-%y %H:%M:%S')


def create_connection():
    return pymysql.connect(
        host=config.host,
        db=config.db,
        user=config.user,
        password=config.password,
        # cursorclass=pymysql.cursors.DictCursor,
        autocommit=True
    )


def insert_data(date_value, country_code, confirmed, deaths, stringency_actual, stringency):
    sql = "replace into statistic  (date_value, country_code, confirmed, deaths, stringency_actual, stringency) VALUES (%s, %s, %s, %s, %s, %s )"
    connection = create_connection()
    try:
        cursor = connection.cursor()
        # Read a single record
        # print(domain)

        # print(domain)
        cursor.execute(sql, (date_value, country_code, confirmed, deaths, stringency_actual, stringency,))
        # result = cursor.fetchall()
        # print(type(result))
        # print(result)
        connection.commit()
        cursor.close()
        return "result"
    finally:
        connection.close()


def select_data():
    sql = "select date_value, country_code, confirmed, deaths, stringency_actual, stringency from statistic  order by date_value;"
    connection = create_connection()
    try:
        cursor = connection.cursor()
        # Read a single record
        # print(domain)

        # print(domain)
        cursor.execute(sql, )
        result = cursor.fetchall()
        print(type(result))
        # print(result)

        cursor.close()
        return result
    finally:
        connection.close()
