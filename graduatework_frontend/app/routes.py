import urllib
from flask import render_template
import json
import urllib.request
import requests

from app import app


@app.route('/')
def get_data():  # put application's code here
    url = "http://backend:3000/api/v1/10day_data"
    response = urllib.request.urlopen(url, timeout=100)
    data = json.loads(response.read())
    # print(data)
    # res = data['data']
    # for date, value in res.items():
    #     val = value
    #     for country, statistic in val.items():
    #         with ThreadPoolExecutor(16) as executor:
    #             result_insert = executor.map(dbConnect.insert_data(date, country, statistic['confirmed'], statistic['deaths'], statistic['stringency_actual'], statistic['stringency']) )
    # data = dbConnect.select_data()
    # print(data)

    return render_template("index.html", data=data);


@app.route('/all_data')
def get_all_data():  # put application's code here
    url = "http://backend:3000/api/v1/all_data"
    response = urllib.request.urlopen(url, timeout=100)
    data = json.loads(response.read())
    # print(data)
    # res = data['data']
    # for date, value in res.items():
    #     val = value
    #     for country, statistic in val.items():
    #         with ThreadPoolExecutor(16) as executor:
    #             result_insert = executor.map(dbConnect.insert_data(date, country, statistic['confirmed'], statistic['deaths'], statistic['stringency_actual'], statistic['stringency']) )
    # data = dbConnect.select_data()
    # print(data)

    return render_template("index_all.html", data=data);


@app.route('/update')
def update_data():  # put application's code here
    url = "http://backend:3000/api/v1/update"
    # response = urllib.request.urlopen(url, timeout=100)
    

    # result = requests.get(url)
    result = requests.get('http://backend:3000/api/v1/update', timeout=300)
    print(result)
    # result = response.read()
    # print(result)
    # res = data['data']
    # for date, value in res.items():
    #     val = value
    #     for country, statistic in val.items():
    #         with ThreadPoolExecutor(16) as executor:
    #             result_insert = executor.map(dbConnect.insert_data(date, country, statistic['confirmed'], statistic['deaths'], statistic['stringency_actual'], statistic['stringency']) )
    # data = dbConnect.select_data()
    # print(data)

    return render_template("index.html", result=result);

@app.route('/update_all')
def update_all_data():  # put application's code here
    url = "http://backend:3000/api/v1/update_all"
    # response = urllib.request.urlopen(url, timeout=100)
    # result = requests.get(url)
    result = requests.get('http://backend:3000/api/v1/update_all', timeout=600)
    # print(result)
    # result = response.read()
    # print(result)
    # res = data['data']
    # for date, value in res.items():
    #     val = value
    #     for country, statistic in val.items():
    #         with ThreadPoolExecutor(16) as executor:
    #             result_insert = executor.map(dbConnect.insert_data(date, country, statistic['confirmed'], statistic['deaths'], statistic['stringency_actual'], statistic['stringency']) )
    # data = dbConnect.select_data()
    # print(data)

    return render_template("index_all.html", result=result);

@app.route('/healthz')
def healthz():
    return "OK"
