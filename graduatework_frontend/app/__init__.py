from flask import Flask
import os
from prometheus_flask_exporter import PrometheusMetrics


app = Flask(__name__)

from app import routes

metrics = PrometheusMetrics(app)