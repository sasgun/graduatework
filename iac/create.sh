#!/bin/bash

PROJECT_ID="35731867"
API_URL="https://gitlab.com/api/v4"
TOKEN_NAME="GRADUATE_WORK"

function CreateVariable
{
#curl --request POST --header "PRIVATE-TOKEN: $GL_API_TOKEN" "$API_URL/projects/$PROJECT_ID/variables/" --form "key=$1" --form "value=$2" --form "protected=false" --form "masked=true" > /dev/null 2>&1
curl --request POST --header "PRIVATE-TOKEN: $GL_API_TOKEN" "$API_URL/projects/$PROJECT_ID/variables/" --form "key=$1" --form "value=Created_variable" --form "protected=false" --form "masked=true"
}



for var in cluster_endpoint cluster_cert_data gitlab_token_develop gitlab_token_production db_password_develop db_password_production db_prod_endpoint db_dev_endpoint; do

	# create vars in gitlab
	CreateVariable ${var^^} 
	 
done

