data "aws_route53_zone" "ashamans_zone" {
  name         = var.domain
  private_zone = false
}

data "kubernetes_service" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx-controller"
    namespace = helm_release.ingress_nginx.metadata[0].namespace
  }
}

data "aws_elb" "ingress_elb" {
  name = regex(
    "(^[^-]+)",
    data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname
  )[0]

  depends_on = [
    helm_release.ingress_nginx
  ]
}


resource "aws_route53_record" "main_domain_record" {
  zone_id = data.aws_route53_zone.ashamans_zone.zone_id
  name    = var.domain
  type    = "A"

  alias {
    name                   = data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname
    zone_id                = data.aws_elb.ingress_elb.zone_id
    evaluate_target_health = true
  }

  depends_on = [
    helm_release.ingress_nginx
  ]
}

resource "aws_route53_record" "dev_domain_record" {
  zone_id = data.aws_route53_zone.ashamans_zone.zone_id
  name    = "dev.${ var.domain}"
  type    = "A"

  alias {
    name                   = data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname
    zone_id                = data.aws_elb.ingress_elb.zone_id
    evaluate_target_health = true
  }

  depends_on = [
    helm_release.ingress_nginx
  ]
}
resource "aws_route53_record" "sonarqube_domain_record" {
  zone_id = data.aws_route53_zone.ashamans_zone.zone_id
  name    = "sonarqube.${ var.domain}"
  type    = "A"

  alias {
    name                   = data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname
    zone_id                = data.aws_elb.ingress_elb.zone_id
    evaluate_target_health = true
  }

  depends_on = [
    helm_release.ingress_nginx
  ]
}
