data "kubernetes_secret" "gitlab_sa_secret_develop" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account_develop.default_secret_name
  }
}

data "kubernetes_secret" "gitlab_sa_secret_production" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account_production.default_secret_name
  }
}


#resource "kubernetes_secret" "docker-registry" {
#  for_each = var.namespaces
#  metadata {
#    name = "imagePull"
#    namespace = each.value
#  }
#
#  data = {
#    ".dockerconfigjson" = "${data.template_file.docker_config_script.rendered}"
#  }
#
#  type = "kubernetes.io/dockerconfigjson"
#}
#
#
#data "template_file" "docker_config_script" {
#  template = "${file("pull-secret.json")}"
#  vars = {
#    docker-username           = "${var.docker-username}"
#    docker-password           = "${var.docker-password}"
#    docker-server             = "${var.docker-server}"
#    docker-email              = "${var.docker-email}"
#    auth                      = base64encode("${var.docker-username}:${var.docker-password}")
#  }
#}
#
