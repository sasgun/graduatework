#=====================REPO_CHARTS==================================

resource "helm_release" "cert_manager" {
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  name       = "cert-manager"

  create_namespace = true
  namespace        = "certmanager"

  set {
    name  = "installCRDs"
    value = "true"
  }

  depends_on = [
	aws_eks_cluster.graduate_work
  ]
}

resource "helm_release" "ingress_nginx" {
  name             = "ingress-nginx"
  namespace        = "ingress"
  create_namespace = true
  chart            = "ingress-nginx"
  version          = "4.0.16"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  timeout          = 150 

  values = [
    <<-EOF
    controller:
      service:
        annotations:
          service.beta.kubernetes.io/aws-load-balancer-type: alb
	  service.beta.kubernetes.io/aws-load-balancer-name: "k8s-cluster"
    EOF
  ]


  depends_on = [
    aws_eks_cluster.graduate_work,
    aws_eks_node_group.workernode
  ]
}

resource "helm_release" "sonarqube" {
  name          = "sonarqube"
  namespace     = "sonarqube"
  repository    = "https://SonarSource.github.io/helm-chart-sonarqube"
  chart         = "sonarqube"
  timeout       = 1800
  force_update  = "true"
  recreate_pods = "true"

  values = [
    "${file("helm_values/sonarqube/values.yaml")}"
  ]

  depends_on = [
    helm_release.cert_manager,
    kubernetes_namespace.sonarqube
  ]

}

