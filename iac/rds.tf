resource "aws_db_parameter_group" "rds_params" {
  name   = "rds-params"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}



resource "aws_db_instance" "graduatework_dev" {

  identifier             = "graduatework-ad-develop"
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t2.micro"
  username               = var.db_user
  password               = random_password.db_password["develop"].result 
  db_name                = var.db_name
  allocated_storage      = 10
  storage_type           = "gp2"
  vpc_security_group_ids = [aws_security_group.rds-sg.id] 
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet.id
  skip_final_snapshot    = true
  identifier_prefix      = null
  multi_az               = false
  storage_encrypted      = false
  snapshot_identifier    = null

  parameter_group_name   = aws_db_parameter_group.rds_params.name

  tags = {
    Name = "DB"
    owner = "aleksandr_dubinin@epam.com"
  }
}


resource "aws_db_instance" "graduatework_prod" {

  identifier             = "graduatework-ad-production"
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t2.micro"
  username               = var.db_user
  password               = random_password.db_password["production"].result
  db_name                = var.db_name
  allocated_storage      = 10
  storage_type           = "gp2"
  vpc_security_group_ids = [aws_security_group.rds-sg.id]
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet.id
  skip_final_snapshot    = true
  identifier_prefix      = null
  multi_az               = false
  storage_encrypted      = false
  snapshot_identifier    = null

  parameter_group_name   = aws_db_parameter_group.rds_params.name

  tags = {
    Name = "DB"
    owner = "aleksandr_dubinin@epam.com"
  }
}


resource "random_password" "db_password" {
  for_each = var.namespaces
  length  = 14 
#  upper   = false # instance names can only have lowercase letters, numbers, and hyphens
  special = false
}

