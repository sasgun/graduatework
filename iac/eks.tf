resource "aws_eks_cluster" "graduate_work" {
  name                      = var.cluster_name
  enabled_cluster_log_types = ["api", "audit"]
  role_arn = aws_iam_role.k8s-cluster.arn
  vpc_config {
    subnet_ids = module.vpc.private_subnets 
  }

  tags = {
              owner = "aleksandr_dubinin@epam.com"
       }

  tags_all = {
              owner = "aleksandr_dubinin@epam.com"
       }
  depends_on = [aws_cloudwatch_log_group.aws_graduate,
		aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
		]
}


resource "aws_eks_node_group" "workernode" {
  cluster_name    = aws_eks_cluster.graduate_work.name
  node_group_name = "node"
  subnet_ids      = module.vpc.private_subnets 
  instance_types  = [ "t3a.small" ]
  node_role_arn   = aws_iam_role.eks_node_role.arn

  scaling_config {
    desired_size = 2
    max_size     = 3
    min_size     = 1
  }

  update_config {
    max_unavailable = 1
  }


  tags = {
              owner = "aleksandr_dubinin@epam.com"
       }

  tags_all = {
              owner = "aleksandr_dubinin@epam.com"
       }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]

}


data "aws_eks_cluster" "cluster" {
  name = aws_eks_cluster.graduate_work.name
}

data "aws_eks_cluster_auth" "cluster" {
  name = aws_eks_cluster.graduate_work.name
}



resource "aws_cloudwatch_log_group" "aws_graduate" {
  # The log group name format is /aws/eks/<cluster-name>/cluster
  # Reference: https://docs.aws.amazon.com/eks/latest/userguide/control-plane-logs.html
  name              = "/aws/eks/${var.cluster_name}/cluster"
  retention_in_days = 7

}
