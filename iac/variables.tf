variable "cluster_name" {
  default     = "graduate-k8s-cluster"
}

variable "node_count" {
  default     = "3"
}

variable "ingressnode_count" {
  default     = "2"
}


variable "db_user" {
  default     = "test"
}

variable "db_pass" {
  default     = "testdfhjdsvdl!"
}

variable "db_name" {
  type    = string
  default = "gradiate_db"
}

variable "namespaces" {
  type    = set(string)
  default = ["production", "develop"]
}


variable "region" {
  default     = "eu-central-1"
}

variable "az_list" {
  default     = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  type        = list(any)
}

variable "public_subnet_cidrs" {
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  type        = list(any)
}

variable "private_subnet_cidrs" {
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  type        = list(any)
}


variable "domain" {
  type    = string
  default = "graduate.ashamans.ru"
}

#
#variable "docker-username" {
#}
#
#variable "docker-password" {
#}
#
#variable "docker-server" {
#}
#
#variable "docker-email" {
#}
